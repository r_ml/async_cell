# {{crate}}

{{badges}}
[![Latest Version](https://img.shields.io/crates/v/async_cell.svg)](https://crates.io/crates/async_cell)
[![Documentation](https://img.shields.io/badge/docs.rs-async__cell-66c2a5)](https://docs.rs/async_cell)
---

{{readme}}
